"use strict";

class ChatMessage {
    constructor(chatMessageId, senderUserId, senderUsername, message, utcTimestamp) {
        this.chatMessageId = chatMessageId;
        this.senderUserId = senderUserId;
        this.senderUsername = senderUsername;
        this.message = message;
        this.utcTimestamp = utcTimestamp;
    }
}

module.exports = ChatMessage;