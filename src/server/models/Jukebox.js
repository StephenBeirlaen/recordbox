"use strict";

let mongoose = require("mongoose"),
    JukeboxSchema = require("../scripts/db/schemas/Jukebox");

let Jukebox = mongoose.model('Jukebox', JukeboxSchema, "jukeboxes"); //model-schema-collection

module.exports = Jukebox;