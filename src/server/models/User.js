"use strict";

class User {
    constructor(userName, jukeboxId) {
        this.userName = userName;
        this.jukeboxId = jukeboxId;
    }
}

module.exports = User;