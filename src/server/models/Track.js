"use strict";

let mongoose = require("mongoose"),
    TrackSchema = require("../scripts/db/schemas/Track");

let Track = mongoose.model('Track', TrackSchema, "tracks"); //model-schema-collection

module.exports = Track;