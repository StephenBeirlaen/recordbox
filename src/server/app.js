"use strict";

let ErrorHandler = require("./scripts/ErrorHandler"),
    StaticServer = require("./scripts/StaticServer"),
    SocketIO = require('socket.io'),
    Sockets = require("./scripts/Sockets"),
    DBService = require("./scripts/db/DBService"),
    Mongoose = require('mongoose');

process.on("uncaughtException", function (err) {
    // wegschrijven naar logfile
    ErrorHandler.writeToErrorLog(err, function (info) {
        console.log("Error occurred: ", info);
    });
});

var staticServer;
var sockets;

(function init() {
    staticServer = new StaticServer();
    var httpServer;
    if (!process.env.PORT) {
        httpServer = staticServer.init(80, "0.0.0.0");
    }
    else {
        httpServer = staticServer.init(process.env.PORT, null);
    }

    var io = SocketIO(httpServer);
    sockets = new Sockets(io);
    sockets.init();

    var DBconnection = new DBService;
    DBconnection.connect('mongodb://dbadmin:hNswGtl89zFeFxVGkB0G@ds141358.mlab.com:41358/recordbox', Mongoose);
})();
