﻿/*global describe, it, before, beforeEach, after, afterEach */
// asynchroon testen met mocha en "done"

let assert = require("assert"),
    request = require("request"),
    should = require("should"),
    DBService = require("../../scripts/db/DBService"),
    JukeboxAPI = require("../../scripts/JukeboxAPI"),
    TrackAPI = require("../../scripts/TrackAPI"),
    Mongoose = require('mongoose'),
    expect = require('expect'),
    Jukebox = require("../../models/Jukebox"),
    Track = require("../../models/Track"),
    Guid = require("guid"),
    YouTubeHelper = require("../../scripts/YouTubeHelper"),
    io = require('socket.io-client');

describe('router', function () {
    beforeEach(function () {
        //calculator = new Calculator();
    });

    describe('file serving', function () {
        it('should serve the favicon', function (done) {
            request.get('http://localhost:80/assets/favicon-16x16.png', function (err, res, body){
                assert.equal(res.statusCode, 200);
                done();
            });
        });
    });

    describe('api', function () {
        it('should return the jukeboxes', function (done) {
            request.get('http://localhost:80/api/jukeboxes', function (err, res, body){
                assert.equal(res.statusCode, 200);
                data = JSON.parse(body);
                data.length.should.be.above(0);
                data[0].should.have.properties(["_id", "name", "userCount", "createdOn"]);
                done();
            });
        });

        it('should add a jukebox', function (done) {
            let jukeboxNameGuid = Guid.raw();
            var options = {
                uri: 'http://localhost:80/api/jukeboxes',
                method: 'POST',
                json: {
                    name: "Test jukebox " + jukeboxNameGuid
                }
            };
            request.post(options, function (err, res, body) {
                assert.equal(res.statusCode, 200);
                //data = JSON.parse(body);
                data = body;
                data.name.should.be.equal("Test jukebox " + jukeboxNameGuid);
                data.should.have.properties(["_id", "name", "userCount", "createdOn"]);
                done();
            });
        });

        it('should return the 10 most upvoted tracks', function (done) {
            request.get('http://localhost:80/api/tracks?order=upvotes', function (err, res, body){
                assert.equal(res.statusCode, 200);
                data = JSON.parse(body);
                data.length.should.be.above(0);
                data[0].should.have.properties(["_id", "title", "youtubeId", "length", "voteCount", "playCount", "createdOn"]);
                done();
            });
        });

        it('should return the 10 most played tracks', function (done) {
            request.get('http://localhost:80/api/tracks?order=plays', function (err, res, body){
                assert.equal(res.statusCode, 200);
                data = JSON.parse(body);
                data.length.should.be.above(0);
                data[0].should.have.properties(["_id", "title", "youtubeId", "length", "voteCount", "playCount", "createdOn"]);
                done();
            });
        });
    });
});

describe('api_classes', function () {
    before(function () {
        var DBconnection = new DBService;
        DBconnection.connect('mongodb://dbadmin:hNswGtl89zFeFxVGkB0G@ds141358.mlab.com:41358/recordbox', Mongoose);
    });

    describe('jukebox api', function () {
        it('should return a list of jukeboxes', function (done) {
            jukeboxAPI = new JukeboxAPI();
            jukeboxAPI.on("result", function (data) {
                data.length.should.be.above(0);
                data[0].should.be.an.instanceof(Jukebox);
                done();
            });
            jukeboxAPI.on("error", function (err) {});
            jukeboxAPI.getJukeboxes();
        });

        it('should add a jukebox', function (done) {
            let jukeboxNameGuid = Guid.raw();
            jukeboxAPI = new JukeboxAPI();
            jukeboxAPI.on("result", function (data) {
                data.name.should.be.equal("Test jukebox " + jukeboxNameGuid);
                data.should.be.an.instanceof(Jukebox);
                done();
            });
            jukeboxAPI.on("error", function (err) {});
            jukeboxAPI.addJukebox("Test jukebox " + jukeboxNameGuid);
        });

        it('should return the 10 most upvoted tracks', function (done) {
            trackAPI = new TrackAPI();
            trackAPI.on("result", function (data) {
                data.length.should.be.exactly(10);
                data[0].should.be.an.instanceof(Track);
                done();
            });
            trackAPI.on("error", function (err) {});
            trackAPI.getTracksByUpvotes();
        });

        it('should return the 10 most played tracks', function (done) {
            trackAPI = new TrackAPI();
            trackAPI.on("result", function (data) {
                data.length.should.be.exactly(10);
                data[0].should.be.an.instanceof(Track);
                done();
            });
            trackAPI.on("error", function (err) {});
            trackAPI.getTracksByPlays();
        });
    });
});

describe('sockets.io', function () {

    var socket;

    before(function () {
        socket = io.connect('http://localhost:80');
    });

    it('should be able to connect', function (done) {
        request.get('http://localhost:80/api/jukeboxes', function (err, res, body){
            let data = JSON.parse(body);
            let jukeBoxId = data[0]._id; // de eerste jukebox ophalen
            let login = {userName : "Unit Test user", jukeBoxId : jukeBoxId};

            socket.on("jukeboxName", (json) => {
                let parsed = JSON.parse(json);
                parsed.jukeboxName.should.equal(data[0].name);

                socket.disconnect();
                done();
            });

            socket.emit("login", login);
        });
    });
});

describe('youtubeHelper', function () {
    it('should return track information', function (done) {
        let youtubeHelper = new YouTubeHelper();
        youtubeHelper.emitter.once("trackInformationResult", function (title, duration) {
            duration.should.equal(228);
            title.should.equal("Friday - Rebecca Black - Official Music Video");
            done();
        });
        youtubeHelper.getTrackInformation("kfVsfOSbJY0");
    });
});