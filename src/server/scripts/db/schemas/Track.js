﻿"use strict";

let mongoose = require('mongoose');

let checkLength = function (val) {
    if (val && val.length > 1 && val.length < 100) {
        return true;
    } else {
        return false;
    }
};

let TrackSchema = new mongoose.Schema({
    title: {
        type: String,
        index: true,
        required: true,
        validate: {
            validator: checkLength,
            message: "Naam moet tussen 1 en 100 karakters lang zijn"
        }
    },
    youtubeId: {
        type: String,
        unique : true,
        required: true
    },
    length: {
        type: Number,
        required: true
    },
    voteCount: {
        type: Number,
        required: true
    },
    playCount: {
        type: Number,
        required: true
    },
    createdOn: {
        type: Date,
        required: true,
        'default': Date.now
    }
});

module.exports = TrackSchema;