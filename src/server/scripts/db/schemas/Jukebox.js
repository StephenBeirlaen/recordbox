﻿"use strict";

let mongoose = require('mongoose');

let checkLength = function (val) {
    if (val && val.length > 1 && val.length < 50) {
        return true;
    } else {
        return false;
    }
};

let JukeboxSchema = new mongoose.Schema({
    name: {
        type: String,
        index: true,
        required: true,
        validate: {
            validator: checkLength,
            message: "Naam moet tussen 1 en 50 karakters lang zijn"
        }
    },
    userCount: {
        type: Number,
        required: true
    },
    createdOn: {
        type: Date,
        required: true,
        'default': Date.now
    }
});

module.exports = JukeboxSchema;