"use strict";

let mongoose = require("mongoose"),
    Track = require("../../../models/Track");

class JukeboxRepo {
    constructor() {

    }

    static getAllTracks(callback) {
        Track.find({}).sort('title').exec(function (err, docs) {
            if (err) { console.log(err); }
            callback(null, docs);
        });
    }

    static getTrackById(id, callback) {
        Track.findOne({'_id': id}).sort('title').exec(function (err, doc) {
            if (err) { console.log(err); }
            callback(null, doc);
        });
    }

    static getTrackByYoutubeId(youtubeId, callback) {
        Track.findOne({'youtubeId': youtubeId}).sort('title').exec(function (err, doc) {
            if (err) { console.log(err); }
            callback(null, doc);
        });
    }

    static getTracksByUpvotes(callback) {
        Track.find({}).sort({'voteCount': -1}).limit(10).exec(function (err, docs) {
            if (err) { console.log(err); }
            callback(null, docs);
        });
    }

    static getTracksByPlays(callback) {
        Track.find({}).sort({'playCount': -1}).limit(10).exec(function (err, docs) {
            if (err) { console.log(err); }
            callback(null, docs);
        });
    }

    static addTrack(title, youtubeId, length, callback) {
        Track.create({
            title: title,
            youtubeId: youtubeId,
            length: length,
            voteCount: 0,
            playCount: 0,
            createdOn: Date.now()
        }, function (err, result) {
            if (err) {
                callback(err, null);
            }
            else {
                callback(null, result);
            }
        });
    }

    static upVoteTrack(trackId, callback) {
        Track.update({'_id': trackId}, {'$inc': { 'voteCount': 1 }}, { multi: false }, function (err, doc) {
            if (err) { console.log(err); }
            callback(null, doc);
        });
    }

    static raiseTrackPlayCount(trackId, callback) {
        Track.update({'_id': trackId}, {'$inc': { 'playCount': 1 }}, { multi: false }, function (err, doc) {
            if (err) { console.log(err); }
            callback(null, doc);
        });
    }
}

module.exports = JukeboxRepo;