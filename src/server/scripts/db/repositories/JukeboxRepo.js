"use strict";

let mongoose = require("mongoose"),
    Jukebox = require("../../../models/Jukebox");

class JukeboxRepo {
    constructor() {

    }

    static getAllJukeboxes(callback) {
        Jukebox.find({}).sort('name').exec(function (err, docs) {
            if (err) { console.log(err); }
            callback(null, docs);
        });
    }

    static getJukeboxById(id, callback) {
        Jukebox.findOne({'_id': id}).sort('name').exec(function (err, doc) {
            if (err) { console.log(err); }
            callback(null, doc);
        });
    }

    static addJukebox(name, callback) {
        Jukebox.create({
            name: name,
            userCount: 0,
            createdOn: Date.now()
        }, function (err, result) {
            if (err) { console.log(err); }
            callback(null, result);
        });
    }
}

module.exports = JukeboxRepo;