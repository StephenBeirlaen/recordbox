﻿"use strict";

class DBService {
    constructor() {

    }

    connect(configURL, database) {
        //var mongoose = database;
        database.Promise = global.Promise; // voor http://stackoverflow.com/questions/38138445/node3341-deprecationwarning-mongoose-mpromise
        var db = database.connect(configURL);// connecteer de database

        database.connection.on("open", function () {
            //mongoose test:
            var msg = "Connected to mongoDB server: " + configURL;

            //collection (=table) names met data als test (obj heeft geen lengte)
            var collections = database.connection.collections;
            msg += "\n \t with known collections/models: ";
            for (var property in collections) {
                msg += collections[property].name + ", ";
            }

            console.log(msg);
        });

        database.connection.on("error", function (error) {
            console.log("MongoDB connection error: ", error.message);
        });

        database.connection.on("close", function () {
            console.log("MongoDB connection closed: ", configURL);
        });

        return database; // mongoose connected
    }
}

module.exports = DBService;