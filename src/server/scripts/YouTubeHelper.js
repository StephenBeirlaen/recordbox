"use strict";

let EventEmitter = require('events').EventEmitter,
    https = require("https");

class YouTubeHelper {

    constructor() {
        this.emitter = new EventEmitter();
    }

    getTrackInformation(youtubeId) {
        let optionsAPI = {
            method: "GET",
            port: 443,
            hostname: "www.googleapis.com",
            path: "/youtube/v3/videos?id=" + youtubeId + "&key=AIzaSyCn_GoMj_rVw-tJ1isv2clWV9Q45Nx80ks&part=snippet,contentDetails&fields=items(id,snippet(title),contentDetails(duration))"

        };
        let convertYoutubeTimestampToSeconds = function (duration) { // http://stackoverflow.com/questions/22148885/converting-youtube-data-api-v3-video-duration-format-to-seconds-in-javascript-no
            let a = duration.match(/\d+/g);

            if (duration.indexOf('M') >= 0 && duration.indexOf('H') == -1 && duration.indexOf('S') == -1) {
                a = [0, a[0], 0];
            }

            if (duration.indexOf('H') >= 0 && duration.indexOf('M') == -1) {
                a = [a[0], 0, a[1]];
            }
            if (duration.indexOf('H') >= 0 && duration.indexOf('M') == -1 && duration.indexOf('S') == -1) {
                a = [a[0], 0, 0];
            }

            duration = 0;

            if (a.length == 3) {
                duration = duration + parseInt(a[0]) * 3600;
                duration = duration + parseInt(a[1]) * 60;
                duration = duration + parseInt(a[2]);
            }

            if (a.length == 2) {
                duration = duration + parseInt(a[0]) * 60;
                duration = duration + parseInt(a[1]);
            }

            if (a.length == 1) {
                duration = duration + parseInt(a[0]);
            }
            return duration
        };
        https.request(optionsAPI, function (response) {
            // we krijgen een stream binnen van die request
            var json = "";

            response.on("data", function (chunk) {
                json += chunk;
            });

            response.on("end", function () {
                var jsonObject = JSON.parse(json);
                if (!jsonObject)
                    return;
                if (jsonObject.hasOwnProperty("items") &&
                    jsonObject.items[0] != null &&
                    jsonObject.items[0].hasOwnProperty("snippet") &&
                    jsonObject.items[0].snippet.hasOwnProperty("title") &&
                    jsonObject.items[0].contentDetails.hasOwnProperty("duration")) {
                    this.emitter.emit("trackInformationResult",
                        jsonObject.items[0].snippet.title,
                        convertYoutubeTimestampToSeconds(jsonObject.items[0].contentDetails.duration)
                    );
                }
            }.bind(this));

            // niet vergeten bij streams:
            response.on("error", function (err) {
                console.log(err);
            });
        }.bind(this)).end(); // IMPORTANT !!! niet vergeten
    }
}

module.exports = YouTubeHelper;
