"use strict";

let fs = require("fs"),
    errorFilename = __dirname + "/../resources/error.log";

class ErrorHandler {
    constructor() {

    }

    static writeToErrorLog(err, cb) {
        this.printerr = err;

        if (err.stack !== null) {
            this.printerr = err.stack;
        }

        let writeBuffer = new Buffer(
            "----------------------------------------------------------------------------------------------"
            + "\n"
            + "["
            + new Date().toLocaleString()
            + "] "
            + this.printerr
            + "\n");

        fs.appendFile(errorFilename, writeBuffer, function(err) {
            if(err) {
                cb(err);
            }

            cb("Log file was saved!");
        });
    }
}

module.exports = ErrorHandler;
