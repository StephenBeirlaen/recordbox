"use strict";

let User = require('./../models/User'),
    Track = require('./../models/Track'),
    TrackRepo = require("../scripts/db/repositories/TrackRepo"),
    JukeboxRepo = require("../scripts/db/repositories/JukeboxRepo"),
    YouTubeHelper = require("./YouTubeHelper");

let allUsers = [];
let jukeboxes = []; // array die temporary info over jukeboxes bijhoudt

class Sockets {
    constructor(io) {
        this.io = io;
    }

    init() {
        let io = this.io;
        let youtubeHelper = new YouTubeHelper();
        let sockets = this;
        io.sockets.on("connection", function (socket) {
            //<editor-fold desc="User Logs in">
            socket.on("login", function (login) {
                // Create a new user
                let newUser = new User(login.userName, login.jukeBoxId);
                socket.user = newUser;
                // Add the user to the users list
                allUsers.push(newUser);

                // Send a broadcast to notify everyone in the channel that a new user logged in
                // Broadcast this BEFORE the user joined the channel, otherwise he'll also receive the notification
                io.to(newUser.jukeboxId).emit("userJoined",
                    JSON.stringify({user: newUser})
                );

                // Join the sockets to a channel (http://socket.io/docs/rooms-and-namespaces/)
                socket.join(newUser.jukeboxId);
                // Jukebox ID en de gebruiker opslaan in de socket voor later gebruik
                socket.jukeBoxId = newUser.jukeboxId;

                // Send the jukebox name to the user
                JukeboxRepo.getJukeboxById(newUser.jukeboxId, function (err, jukebox) {
                    if (err) { console.log(err); }
                    else {
                        socket.emit("jukeboxName",
                            JSON.stringify({jukeboxName: jukebox.name})
                        );
                    }
                });

                // Broadcast the users of the jukebox
                sockets.broadcastJukeboxUsersToChannel(login.jukeBoxId);

                // Verwijzingen naar track id's worden opgeslagen in de array met voorlopige
                // data over jukeboxes (playlists zijn vluchtig)
                if (jukeboxes[login.jukeBoxId] == null) { // Bestaat er nog geen cache voor deze jukebox?
                    jukeboxes[login.jukeBoxId] = {}; // Cache object aanmaken
                    jukeboxes[login.jukeBoxId].tracks = [];
                }

                if(jukeboxes[socket.jukeBoxId].currentlyPlayingTrack && jukeboxes[login.jukeBoxId].tracks.length > 0){
                    console.log("asking to play");
                    socket.emit("resumePlaying",
                        JSON.stringify({
                            ytId: jukeboxes[socket.jukeBoxId].currentlyPlayingTrack.youtubeId,
                            title: jukeboxes[socket.jukeBoxId].currentlyPlayingTrack.title,
                            timer: jukeboxes[login.jukeBoxId].timer})
                    );
                }

                // Send the tracks in the jukebox to the user
                sockets.sendJukeboxTracksToUser(socket, jukeboxes[login.jukeBoxId].tracks);
            });
            //</editor-fold>

            //<editor-fold desc="Chat Message">
            socket.on("chatMessage", function (message) {
                // Broadcast it to all other users in the channel
                io.to(socket.jukeBoxId).emit("chatMessage",
                    JSON.stringify({userName: socket.user.userName, message: message})
                );
            });
            //</editor-fold>

            //<editor-fold desc="Adding a track">
            socket.on("newTrack", function (data) {
                if(!data)
                    return;

                let json = JSON.parse(data);
                if(json.hasOwnProperty('youtubeId')) {
                    // Get track title and length
                    youtubeHelper.emitter.once("trackInformationResult", function (title, duration) {
                        // Insert a track
                        TrackRepo.addTrack(title, json.youtubeId, duration, function (err, newTrack) {
                            if (!err && newTrack) {
                                // New track inserted
                                sockets.handleNewTrack(socket.jukeBoxId, newTrack);
                            }
                            else {
                                // Maybe the track already exists in the database and there was a unique constraint error?
                                // Try to find the track
                                TrackRepo.getTrackByYoutubeId(json.youtubeId, function (err, track) {
                                    if (err) { console.log(err); }
                                    if (track) {
                                        // An already existing track was found in the database
                                        sockets.handleNewTrack(socket.jukeBoxId, track);
                                    }
                                });
                            }
                        });
                    });
                    youtubeHelper.getTrackInformation(json.youtubeId);
                }
            });
            //</editor-fold>

            //<editor-fold desc="Disconnecting">
            // When a user drops out of the jukebox
            socket.on('disconnect', function () {
                // Remove the user from the list of users
                let index = allUsers.indexOf(socket.user);
                allUsers.splice(index, 1);
                // Broadcast the updated list of jukebox users to all other users in the channel
                sockets.broadcastJukeboxUsersToChannel(socket.jukeBoxId);

                // Send a broadcast to notify everyone in the channel that a new user logged out
                if(socket.user)
                {
                    io.to(socket.jukeBoxId).emit("userLeft",
                        JSON.stringify({user: socket.user.userName})
                    );
                }
            });
            //</editor-fold>

            //<editor-fold desc="Vote System">
            socket.on("voteKickTrack", function (trackId) {
                // Get the track details
                jukeboxes[socket.jukeBoxId].kickInPorgress = true;
                TrackRepo.getTrackById(trackId, function (err, track) {
                    if (err) { console.log(err); }
                    if (track) {
                        // Broadcast it to all other users in the channel asking for their vote
                        io.to(socket.jukeBoxId).emit("voteKickTrack",
                            JSON.stringify({
                                userName: socket.user.userName,
                                trackId: trackId,
                                trackTitle: track.title
                            })
                        );

                        //initiating from votes
                        jukeboxes[socket.jukeBoxId].votes = 0;
                        jukeboxes[socket.jukeBoxId].votesYes = 0;
                        jukeboxes[socket.jukeBoxId].votesNo = 0;

                        //if not all users have voted send kickVoteDone to all jukebox users
                        setTimeout(function () {
                            if(jukeboxes[socket.jukeBoxId].kickInPorgress)
                            {
                                jukeboxes[socket.jukeBoxId].kickInPorgress = false;
                                io.to(socket.jukeBoxId).emit("kickVoteDone",
                                    JSON.stringify({kick: (jukeboxes[socket.jukeBoxId].votesNo < jukeboxes[socket.jukeBoxId].votesYes)}));
                            }
                        }, 15000);
                    }
                });
            });

            socket.on("voteKick", function (data) {
                let json = JSON.parse(data);
                //update votes
                jukeboxes[socket.jukeBoxId].votes++;
                //check for kickvote
                if(json.kick){
                    jukeboxes[socket.jukeBoxId].votesYes++;
                }
                else{
                    jukeboxes[socket.jukeBoxId].votesNo++;
                }
                //check if all users have voted.
                let users = allUsers.filter(function(u) { return u.jukeboxId == socket.jukeBoxId; });
                if(jukeboxes[socket.jukeBoxId].votes == users.length){
                    jukeboxes[socket.jukeBoxId].kickInPorgress = false;
                    let kick = (jukeboxes[socket.jukeBoxId].votesNo < jukeboxes[socket.jukeBoxId].votesYes);
                    if(kick){
                        jukeboxes[socket.jukeBoxId].tracks.find(function (t) {
                            if(t){
                                if(t._id.toString() === json.trackId) {
                                    let index = jukeboxes[socket.jukeBoxId].tracks.indexOf(t);
                                    jukeboxes[socket.jukeBoxId].tracks.splice(index,1);
                                    sockets.broadcastJukeboxTracksToChannel(socket.jukeBoxId, jukeboxes[socket.jukeBoxId].tracks);
                                }
                            }
                        });
                    }
                    io.to(socket.jukeBoxId).emit("kickVoteDone",
                        JSON.stringify({kick: kick})
                    );
                }
            });

            socket.on("voteNextTrack", function () {
                // Broadcast it to all other users in the channel asking for their vote
                io.to(socket.jukeBoxId).emit("voteNextTrack",
                    JSON.stringify({
                        userName: socket.user.userName
                    })
                );

                if (jukeboxes[socket.jukeBoxId] == null) { // Bestaat er nog geen cache voor deze jukebox?
                    jukeboxes[socket.jukeBoxId] = {}; // Cache object aanmaken
                }
                jukeboxes[socket.jukeBoxId].nextVotes = 0;
                jukeboxes[socket.jukeBoxId].nextVotesYes = 0;
                jukeboxes[socket.jukeBoxId].nextVotesNo = 0;

                setTimeout(function () {
                    let users = allUsers.filter(function(u) { return u.jukeboxId == socket.jukeBoxId; });
                    if(jukeboxes[socket.jukeBoxId].nextVotes != users.length){
                        io.to(socket.jukeBoxId).emit("nextTrackVoteDone",
                            JSON.stringify({next: (jukeboxes[socket.jukeBoxId].nextVotesNo < jukeboxes[socket.jukeBoxId].nextVotesYes)})
                        );
                    }
                }, 15000);
            });

            socket.on("voteNext", function (data) {
                let json = JSON.parse(data);
                jukeboxes[socket.jukeBoxId].nextVotes++;
                console.log( jukeboxes[socket.jukeBoxId].nextVotes);
                if(json.next){
                    jukeboxes[socket.jukeBoxId].nextVotesYes++;
                }
                else{
                    jukeboxes[socket.jukeBoxId].nextVotesNo++;
                }
                let users = allUsers.filter(function(u) { return u.jukeboxId == socket.jukeBoxId; });
                if(jukeboxes[socket.jukeBoxId].nextVotes == users.length){
                    let next = (jukeboxes[socket.jukeBoxId].nextVotesNo < jukeboxes[socket.jukeBoxId].nextVotesYes);
                    if(next){
                        // Skip the track
                        let index = sockets.getNextTrackId(socket.jukeBoxId, jukeboxes[socket.jukeBoxId].currentlyPlayingTrack);
                        sockets.playNextTrack(socket.jukeBoxId, jukeboxes[socket.jukeBoxId].tracks[index]);
                    }
                    io.to(socket.jukeBoxId).emit("nextTrackVoteDone",
                        JSON.stringify({next: next})
                    );
                }
            });
            //</editor-fold>
            
            //<editor-fold desc="upvoting track">
            socket.on("upVoteTrack", function (trackId) {
                TrackRepo.upVoteTrack(trackId, function (err, doc) {
                    if (err) console.log("Error upvoting track: " + err);
                });
            });
            //</editor-fold>
        });
    }

    broadcastJukeboxUsersToChannel(jukeBoxId) {
        // Get all users in the jukebox
        let jukeboxUsers = allUsers.filter(function(u) { return u.jukeboxId == jukeBoxId; });
        this.io.to(jukeBoxId).emit("jukeboxUsers",
            JSON.stringify(jukeboxUsers)
        );
    }

    sendJukeboxTracksToUser(socket, jukeboxTracks) {
        socket.emit("jukeboxTracks",
            JSON.stringify({jukeboxTracks: jukeboxTracks})
        );
    }

    broadcastJukeboxTracksToChannel(jukeBoxId, jukeboxTracks) {
        this.io.to(jukeBoxId).emit("jukeboxTracks",
            JSON.stringify({jukeboxTracks: jukeboxTracks})
        );
    }

    handleNewTrack(jukeBoxId, track) {
        // Is the track not yet in the playlist?
        if (!jukeboxes[jukeBoxId].tracks.find(function (t) { return t._id.toString() === track._id.toString(); })) {
            // Add the track to this jukebox's cache array
            jukeboxes[jukeBoxId].tracks.push(track);

            // Is there a track playing?
            if (jukeboxes[jukeBoxId].tracks.length == 1) {
                this.playNextTrack(jukeBoxId, track);
            }

            // Broadcast the tracks to all other users in the channel
            this.broadcastJukeboxTracksToChannel(jukeBoxId, jukeboxes[jukeBoxId].tracks);
        }
    }

    playNextTrack(jukeBoxId, track) {
        this.io.to(jukeBoxId).emit("startPlaying",
            JSON.stringify({ytId: track.youtubeId, title: track.title})
        );
        jukeboxes[jukeBoxId].timer = 0;
        if(jukeboxes[jukeBoxId].interval){
            clearInterval(jukeboxes[jukeBoxId].interval);
        }
        jukeboxes[jukeBoxId].interval = setInterval(()=>{
            jukeboxes[jukeBoxId].timer++;
        },1000);

        jukeboxes[jukeBoxId].currentlyPlayingTrack = track;
        if(jukeboxes[jukeBoxId].playCounter){
            jukeboxes[jukeBoxId].playCounter++;
        }else{
            jukeboxes[jukeBoxId].playCounter = 1;
        }

        //Creating a copy from the object NOT A REFERENCE!
        let currentTrack = JSON.parse(JSON.stringify(jukeboxes[jukeBoxId].currentlyPlayingTrack));
        let currentPlayCounter = JSON.parse(JSON.stringify(jukeboxes[jukeBoxId].playCounter));
        TrackRepo.raiseTrackPlayCount(track._id, function (err, doc) {
            if (err) console.log("Error raising track play count: " + err);
        });

        let delay = track.length < 1 ? 1 : track.length; // minimum delay van 1s instellen (blocking tegengaan)
        setTimeout(function () {
            if(jukeboxes[jukeBoxId].currentlyPlayingTrack._id == currentTrack._id && jukeboxes[jukeBoxId].playCounter == currentPlayCounter){
                console.log("still the same song playing, going for the next track");
                let index = this.getNextTrackId(jukeBoxId, track);
                this.playNextTrack(jukeBoxId, jukeboxes[jukeBoxId].tracks[index]);
            }else{
                console.log("another song is already playing.");
            }
        }.bind(this), delay * 1000);
    }

    getNextTrackId(jukeBoxId, track) {
        // Get the next track
        let index = jukeboxes[jukeBoxId].tracks.indexOf(track);
        if(jukeboxes[jukeBoxId].tracks.length === 0){
            return;
        }
        index++;
        if (index > jukeboxes[jukeBoxId].tracks.length - 1) {
            index = 0;
        }
        return index;
    }
}

module.exports = Sockets;
