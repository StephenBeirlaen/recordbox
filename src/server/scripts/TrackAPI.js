"use strict";

let http = require("http"),
    Track = require("../models/Track"),
    TrackRepo = require("../scripts/db/repositories/TrackRepo");

class TrackAPI {
    constructor() {
        // 1. Eventemitter ophalen
        var EventEmitter = require('events').EventEmitter;
        this.emitter = new EventEmitter();
    }

    getTracksByUpvotes() {
        var self = this;

        TrackRepo.getTracksByUpvotes(function (err, docs) {
            self.emitter.emit("result", docs);
        });
    }

    getTracksByPlays() {
        var self = this;

        TrackRepo.getTracksByPlays(function (err, docs) {
            self.emitter.emit("result", docs);
        });
    }

    on(event, cb) {
        this.emitter.once(event, cb); // ON is niet hetzelfde als ONCE!
    }
}

module.exports = TrackAPI;
