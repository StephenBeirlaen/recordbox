"use strict";

let http = require("http"),
    Jukebox = require("../models/Jukebox"),
    JukeboxRepo = require("../scripts/db/repositories/JukeboxRepo");

class JukeboxAPI {
    constructor() {
        // 1. Eventemitter ophalen
        var EventEmitter = require('events').EventEmitter;
        this.emitter = new EventEmitter();
    }

    getJukeboxes() {
        var self = this;

        JukeboxRepo.getAllJukeboxes(function (err, docs) {
            self.emitter.emit("result", docs);
        });
    }

    addJukebox(name) {
        var self = this;

        JukeboxRepo.addJukebox(name, function (err, result) {
            self.emitter.emit("result", result);
        });
    }

    on(event, cb) {
        this.emitter.once(event, cb); // ON is niet hetzelfde als ONCE!
    }
}

module.exports = JukeboxAPI;
