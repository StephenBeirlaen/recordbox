"use strict";

let fs = require('fs'),
    path = require('path'),
    url = require('url'),
    querystring = require('querystring'),
    JukeboxAPI = require('./JukeboxAPI'),
    TrackAPI = require('./TrackAPI');

let extensions = {
    ".html": "text/html",
    ".css": "text/css",
    ".js": "application/javascript",
    ".png": "image/png",
    ".gif": "image/gif",
    ".jpg": "image/jpeg",
    ".json": "application/json",
    ".svg": "image/svg+xml",
    ".ttf": "application/x-font-ttf",
    ".otf": "application/x-font-opentype",
    ".woff": "application/font-woff",
    ".woff2": "application/font-woff2",
    ".eot": "application/vnd.ms-fontobject",
    ".sfnt": "application/font-sfnt"
};

let HTTP_METHOD = {
    GET: "GET",
    POST: "POST"
};

class Router {
    constructor() {

    }

    static route(req, cb) {
        if (req.url === "/" || req.url === "") {
            req.url = "/index.html";
        }
        let filename = req.url;

        let ext = path.extname(filename),
            localPath = path.normalize(process.cwd() + "/src/server/public/wwwroot" + filename), // cwd = current working directory
            dirName = path.dirname(filename),
            pathName = url.parse(filename).pathname,
            httpMethod = req.method,
            qs = url.parse(filename).query; // query string

        var jukeboxAPI = new JukeboxAPI();
        var trackAPI = new TrackAPI();

        fs.stat(localPath, function (err, stats) {
            if (stats && stats.isFile()) { // is het een file?
                if (extensions[ext]) { // extensie is bekend
                    // lees de file met fs en verzend met correct mimeType
                    Router.readFile(localPath, function (err, content) {
                        if (err) {
                            cb(err, null, null);
                        }
                        else {
                            cb(null, content, extensions[ext]);
                        }
                    });
                }
                else {
                    // extensie niet bekend bij de server
                    cb("Unknown file type", null, null);
                }
            }
            else { // het is geen file, dus misschien is het een API getJukeboxes
                switch (pathName) {
                    case "/api/jukeboxes":
                        switch (httpMethod) {
                            case HTTP_METHOD.GET:
                                jukeboxAPI.on("result", function (data) {
                                    cb(null, JSON.stringify(data), extensions[".json"]);
                                });

                                jukeboxAPI.on("error", function (err) {
                                    cb("Error getting API data: " + err, null, null);
                                });

                                jukeboxAPI.getJukeboxes();
                                break;
                            case HTTP_METHOD.POST: // POST: JSON{name:jukeboxName}
                                let fullBody = '';

                                req.on('data', function(chunk) {
                                    fullBody += chunk.toString();
                                });

                                req.on('end', function() {
                                    let decodedBody = JSON.parse(fullBody);

                                    if (decodedBody &&
                                        decodedBody.hasOwnProperty("name")) {
                                        jukeboxAPI.on("result", function (data) {
                                            cb(null, JSON.stringify(data), extensions[".json"]);
                                        });

                                        jukeboxAPI.on("error", function (err) {
                                            cb("Error adding jukebox: " + err, null, null);
                                        });

                                        jukeboxAPI.addJukebox(decodedBody.name);
                                    }
                                    else {
                                        cb("Error joining jukebox - couldn't parse request body", null, null);
                                    }
                                });

                                req.on("error", function (err) {
                                    cb("Error joining jukebox: " + err, null, null);
                                });
                                break;
                            default:
                                cb("Unknown route", null, null);
                        }
                        break;
                    case "/api/tracks":
                        switch (httpMethod) {
                            case HTTP_METHOD.GET:
                                switch (qs) {
                                    case "order=upvotes":
                                        trackAPI.on("result", function (data) {
                                            cb(null, JSON.stringify(data), extensions[".json"]);
                                        });

                                        trackAPI.on("error", function (err) {
                                            cb("Error getting API data: " + err, null, null);
                                        });

                                        trackAPI.getTracksByUpvotes();
                                        break;
                                    case "order=plays":
                                        trackAPI.on("result", function (data) {
                                            cb(null, JSON.stringify(data), extensions[".json"]);
                                        });

                                        trackAPI.on("error", function (err) {
                                            cb("Error getting API data: " + err, null, null);
                                        });

                                        trackAPI.getTracksByPlays();
                                        break;
                                    default:
                                        cb("Unknown route", null, null);
                                }
                                break;
                            default:
                                //fs.stat(localPath)
                                cb("Unknown route", null, null);
                                break;
                        }
                        break;
                    default:
                        localPath = path.normalize(process.cwd() + "/src/server/public/wwwroot" + "/index.html");
                        Router.readFile(localPath, function (err, content) {
                            if (err) {
                                cb(err, null, null);
                            }
                            else {
                                cb(null, content, extensions[".html"]);
                            }
                        });
                        break;
                }
            }
        });
    }

    static readFile(localPath, cb) {
        fs.readFile(localPath, function (err, content) {
            if (err) {
                cb(err, null);
            }
            else {
                cb(null, content);
            }
        });
    }
}

module.exports = Router;