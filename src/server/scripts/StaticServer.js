"use strict";

let http = require('http'),
    Router = require('./Router');

class StaticServer {
    constructor() {

    }

    init(httpPort, ipAddress) {
        console.log("Server is running on port: ", httpPort, " on ip: ", ipAddress);
        return StaticServer.httpListen(httpPort, ipAddress);
    }

    static httpListen(httpPort, ipAddress) {
        var server = http.createServer(function(req, res) {
            Router.route(req, function (err, data, mimeType) {
                //res.setHeader('Access-Control-Allow-Origin', 'http://' + ipAddress + ':4200');
                res.setHeader('Access-Control-Allow-Origin', '*');
                res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
                res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
                res.setHeader('Access-Control-Allow-Credentials', true);
                // Deze functie verzorgt de res.write
                if (err) {
                    res.writeHead(500);
                    res.write("Server error 500: can't read file \n" + err);
                    res.end();
                    console.log("Error: " + req.method + " " + req.url + " [500]");
                    console.log(process.cwd());
                }
                else {
                    res.writeHead(200, { 'Content-Type': mimeType });
                    res.write(data);
                    res.end();
                    console.log("Serving: " + req.method + " " + req.url + " [200]");
                }
            });
        });
        if (ipAddress) {
            server.listen(httpPort, ipAddress);
        }
        else {
            server.listen(httpPort);
        }
        return server;
    }
}

module.exports = StaticServer;