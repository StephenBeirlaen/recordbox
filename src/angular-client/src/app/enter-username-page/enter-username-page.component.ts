import { Component,ViewEncapsulation } from '@angular/core';
import {Router, Params, ActivatedRoute} from "@angular/router";
import {UserService} from "../services/user.service";

@Component({
    selector: 'enter-username-page',
    templateUrl: 'enter-username-page.component.html',
    styleUrls: ['enter-username-page.component.scss'],
    encapsulation: ViewEncapsulation.None

})
export class EnterUsernamePageComponent{

    displayName;

    constructor(private route: ActivatedRoute, private router: Router, private userService: UserService){

    }

    joinJukebox(){
        if(this.displayName.replace(/\s+/, "").length < 5){
            window.alert("Name should be more than 5 characters.");
        }
        else{
            this.userService.setUserName(this.displayName);
            let jukeBoxId = this.route.snapshot.params['id'];
            this.router.navigate(['/jukeboxes/', jukeBoxId]);
        }
    }
}