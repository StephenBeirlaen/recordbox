import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router'

import { AppComponent } from './root/app.component';
import { BrowseRoomsComponent } from './browse-rooms/browse-rooms.component';
import { AppRoutingModule } from "./routers/app.router";
import { HomePageComponent } from "./home-page/home-page.component";
import { LoginRoomComponent } from "./login-room/login-room.component";
import { ApiClientService } from "./services/api-client.service";
import { RegisterRoomComponent } from "./register-room/register-room.component";
import { JukeboxService } from "./services/jukebox.service";
import { OneRoomComponent } from "./one-room/one-room.component";
import { ChatService } from "./services/chat.service";
import { YoutubePlayerModule } from 'ng2-youtube-player';
import {UserService} from "./services/user.service";
import {SocketService} from "./services/socket.service";
import {TrackService} from "./services/track.service";
import {YoutubeComponent} from "./youtube/youtube.component";
import {LoginGuard} from "./guards/login.guard";
import {TracksComponent} from "./tracks/tracks.component";
import {ActionsComponent} from "./actions/actions.component";
import {AddTracksComponent} from "./add-tracks/add-tracks.component";
import {YouTubeService} from "./services/youtube.service";
import {StatsComponent} from "./stats/stats.component";
import {EnterUsernamePageComponent} from "./enter-username-page/enter-username-page.component";

@NgModule({
    declarations: [
        AppComponent,
        HomePageComponent,
        BrowseRoomsComponent,
        LoginRoomComponent,
        RegisterRoomComponent,
        OneRoomComponent,
        YoutubeComponent,
        TracksComponent,
        ActionsComponent,
        AddTracksComponent,
        StatsComponent,
        EnterUsernamePageComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        RouterModule,
        AppRoutingModule,
        YoutubePlayerModule
    ],
    providers: [ApiClientService, JukeboxService, ChatService, UserService, TrackService, SocketService, YouTubeService, LoginGuard],
    bootstrap: [AppComponent]
})
export class AppModule { }
