import { Injectable } from '@angular/core';
import {Router, CanActivate, ActivatedRoute, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { UserService } from 'app/services/user.service';
import {Observable} from "rxjs";
import { Promise } from "es6-promise";

@Injectable()
export class LoginGuard implements CanActivate {

    constructor(private userService: UserService, private router: Router, private route: ActivatedRoute) {

    }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): boolean {
        if(this.userService.username){
            return true;
        }else{
            this.router.navigate(['/enter_username/', route.params["id"]]);
            return false;
        }
    }
}