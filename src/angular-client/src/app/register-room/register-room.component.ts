import { Component, OnInit } from '@angular/core';

import {ApiClientService} from "../services/api-client.service";
import {JukeboxService} from "../services/jukebox.service";
import {UserService} from "../services/user.service";
import {Router} from "@angular/router";

@Component({
    selector: 'register-room',
    templateUrl: './register-room.component.html',
    styleUrls: ['./register-room.component.scss']
})
export class RegisterRoomComponent{
    apiService : ApiClientService;
    jukeBoxService: JukeboxService;

    jukeboxName : string;
    newRoomDisplayName : string;

    constructor(apiService : ApiClientService, jukeBoxService: JukeboxService, private userService: UserService, private router: Router){
        this.apiService = apiService;
        this.jukeBoxService = jukeBoxService;
    }

    createBox(){
        if(!this.jukeboxName || !this.newRoomDisplayName){
            window.alert("Some fields are not filled in!");
        }else if(this.jukeboxName.replace(/\s+/, "").length < 5){
            window.alert("JukeBox name should be more than 5 characters");
        }else if(this.newRoomDisplayName.replace(/\s+/, "").length < 5){
            window.alert("Name should be more than 5 characters.");
        }
        else{
            this.jukeBoxService.emitter.subscribe(event => {
                switch (event.eventName) {
                    case "jukeboxCreated":
                        //do something to join the jukebox
                        this.userService.setUserName(this.newRoomDisplayName);
                        this.router.navigate(['/jukeboxes/', event.jukeboxId]);
                        console.log("logged in with :", this.newRoomDisplayName);
                        break;
                }
            });

            this.jukeBoxService.createNewJukebox(this.jukeboxName);
        }
    }
}