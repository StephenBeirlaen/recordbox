import {Injectable, EventEmitter} from '@angular/core';
import {Router} from "@angular/router";
import {SocketService} from "./socket.service";

@Injectable()
export class UserService {
    public username: string = null;
    public users: any[];
    usersEmitter: EventEmitter<any> = new EventEmitter();

    constructor(private socketService : SocketService, private router: Router){
        this.users = [];
        this.socketService.usersEmitter.subscribe(event => {
            switch (event.eventName) {
                case "newUserList" :
                    console.log("new user list");
                    this.clearUsers();
                    this.users = event.list;
                    console.log(this.users);
                    this.usersEmitter.emit({eventName:'userList', users: this.users});
                    break;
                case "newUser" :
                    this.users.push(event.user);
                    console.log("new user");
                    console.log(this.users);
                    this.usersEmitter.emit({eventName:'newUser', user: event.user});
                    break;
                case "userLeft" :
                    let index = this.users.indexOf(event.user);
                    this.users.splice(index, 1);
                    console.log("user left", event.user);
                    this.usersEmitter.emit({eventName:'userLeft', user: event.user});
                    break;
            }
        });
    }

    login(userName, jukeBoxId) {
        this.socketService.initiateSocket();
        console.log("logging in to the jukebox with", userName);
        let login = {userName : userName, jukeBoxId : jukeBoxId};
        this.socketService.sendToSocket("login", login);
        this.socketService.listenForUserListOnce();
    }

    setUserName(userName: string) :void {
        this.username = userName;
    }

    getAllUsersFromRoom(){
        return this.users;
    }

    clearUsers(){
        console.log('clearing users');
        this.users = [];
    }

    logout(){
        this.username = null;
        this.clearUsers();
        this.socketService.disconnect();
        this.router.navigate(['']);
    }
}