import {Injectable, EventEmitter} from '@angular/core';
import * as io from 'socket.io-client';
import {environment} from "../../environments/environment";
import {UserService} from "./user.service";
import {TrackService} from "./track.service";

@Injectable()
export class SocketService {
    private url = environment.socketURL;
    public socket;
    musicEmitter: EventEmitter<any> = new EventEmitter();
    usersEmitter: EventEmitter<any> = new EventEmitter();
    jukeboxEmitter: EventEmitter<any> = new EventEmitter();
    chatEmitter: EventEmitter<any> = new EventEmitter();

    constructor() {
    }

    initiateSocket(){
        //initiate socket
        this.socket = io(this.url);

        // Listen for the jukebox name
        this.socket.on('jukeboxName', (json) => {
            let params = JSON.parse(json);
            this.jukeboxEmitter.emit({ eventName: "newJukeboxName", jukeboxName: params.jukeboxName});
        });

        this.listenForUserListOnce();

        this.socket.on("userJoined", (json) => {
            let data = JSON.parse(json);
            this.usersEmitter.emit({eventName:'newUser', user:data.user});
        });

        this.socket.on("userLeft", (json) => {
            let data = JSON.parse(json);
            console.log(data);
            this.usersEmitter.emit({eventName:'userLeft', user:data.user});
        });

        this.socket.on('chatMessage', (json) =>
        {
            let params = JSON.parse(json);
            this.chatEmitter.emit({ eventName: "chatMessageReceived", userName: params.userName, message: params.message});
        });

        //listen for jukeboxTrackUpdates
        this.socket.on("jukeboxTracks", (json) => {
            let tracks = (JSON.parse(json)).jukeboxTracks;
            this.musicEmitter.emit({eventName:'newTrackList', list:tracks})

        });

        this.socket.on("startPlaying", (json) => {
            let params = JSON.parse(json);
            this.musicEmitter.emit({eventName:'startPlaying', ytId:params.ytId, title: params.title});
        });

        this.socket.on("resumePlaying", (json) => {
            let params = JSON.parse(json);
            this.musicEmitter.emit({eventName:'resumePlaying', ytId:params.ytId, title: params.title, timer: params.timer});
        });

        this.socket.on("voteKickTrack", (json) => {
            let params = JSON.parse(json);
            this.musicEmitter.emit({eventName:'voteKickTrack', userName:params.userName, trackId:params.trackId, trackTitle:params.trackTitle});
        });

        this.socket.on("kickVoteDone", (json) => {
            let data = JSON.parse(json);
            this.musicEmitter.emit({eventName:'kickVoteDone', kick:data.kick});
        });

        this.socket.on("voteNextTrack", (json) => {
            let params = JSON.parse(json);
            this.musicEmitter.emit({eventName:'voteNextTrack', userName:params.userName});
        });

        this.socket.on("nextTrackVoteDone", (json) => {
            let data = JSON.parse(json);
            this.musicEmitter.emit({eventName:'nextTrackVoteDone', next:data.next});
        });
    }

    sendToSocket(eventName:string, data){
        if(!this.socket.connected){
            this.initiateSocket();
        }
        this.socket.emit(eventName, data);
    }

    disconnect() {
        this.socket.disconnect();
    }

    listenForUserListOnce(){
        this.socket.once("jukeboxUsers", (json) => {
            let userList = JSON.parse(json);
            console.log(userList);
            this.usersEmitter.emit({eventName:'newUserList', list:userList});
        });
    }
}