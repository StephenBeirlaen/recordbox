import { Injectable } from '@angular/core';
import {Http, Headers, Response} from '@angular/http';

import 'rxjs/add/operator/map'
import {Observable} from "rxjs";
import {YouTubeSearchResult} from "../models/youTubeSearchResult";

@Injectable()
export class YouTubeService {

    constructor(private http: Http){

    }

    searchYoutube(query): Observable<YouTubeSearchResult[]> {
        return this.http.get(encodeURI("https://www.googleapis.com/youtube/v3/search?part=snippet&type=video&q=" + query + "&key=AIzaSyCn_GoMj_rVw-tJ1isv2clWV9Q45Nx80ks"))
            .map(this.mapSearchResults)
    }

    mapSearchResults = (response:Response): YouTubeSearchResult[] =>{
        return response.json().items.map(this.toSearchResult)
    };

    toSearchResult = (item: any): YouTubeSearchResult => {
        let youTubeSearchResult = <YouTubeSearchResult>({
            youtubeId: item.id.videoId,
            title: item.snippet.title,
            description: item.snippet.description,
            thumbnailUrl: item.snippet.thumbnails.default.url,
        });
        console.log('Parsed result:', youTubeSearchResult);
        return youTubeSearchResult;
    };
}