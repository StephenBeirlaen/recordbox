import {Injectable, EventEmitter} from '@angular/core';
import {environment} from "../../environments/environment";
import {ApiClientService} from "./api-client.service";
import {SocketService} from "./socket.service";

@Injectable()
export class TrackService {
    public tracks: any[];
    tracksEmitter: EventEmitter<any> = new EventEmitter();
    playerEmitter: EventEmitter<any> = new EventEmitter();

    /*trackToKickId;
    trackToKickTitle;*/

    constructor(private socketService: SocketService){
        this.tracks = [];

        this.socketService.musicEmitter.subscribe(event => {
            if(event.eventName == "newTrackList"){
                this.clearTracks();
                for(let track in event.list){
                    this.addTrackToLocalPlaylist(event.list[track]);
                }
            }
            if(event.eventName == "startPlaying"){
                this.playerEmitter.emit({eventName:"startPlaying", trackId:event.ytId, title: event.title});
            }
            if(event.eventName == "resumePlaying"){
                this.playerEmitter.emit({eventName:"resumePlaying", trackId:event.ytId, title: event.title, timer: event.timer});
            }
        });

        this.socketService.musicEmitter.subscribe(event => {
            if (event.eventName == "voteKickTrack") {
                this.tracksEmitter.emit({eventName: "voteKickTrack", userName: event.userName, trackId: event.trackId, trackTitle:event.trackTitle });
                /*this.trackToKickId = event.trackId;
                this.trackToKickTitle = event.trackTitle;*/
            }
            if (event.eventName == "kickVoteDone") {
                if (event.kick) {
                    console.log('the song has been kicked');
                    this.tracksEmitter.emit({eventName:'newTrackList' ,tracks: this.tracks});
                }
                this.tracksEmitter.emit({eventName: 'kickVoteDone',kick:event.kick});
            }
            if (event.eventName == "voteNextTrack") {
                this.tracksEmitter.emit({eventName: "voteNextTrack", userName: event.userName,trackTitle:event.trackTitle });
            }
            if (event.eventName == "nextTrackVoteDone") {
                if (event.next) {
                    console.log('the current song was skipped');
                }
                this.tracksEmitter.emit({eventName: 'nextTrackVoteDone',trackTitle:event.trackTitle,next:event.next})
            }
        });
    }

    private addTrackToLocalPlaylist(track){
        this.tracks.push(track);
        this.tracksEmitter.emit({eventName:'newTrackList' ,tracks: this.tracks});
    }

    addTrackToPlaylist(id){
        let data = {youtubeId: id};
        this.socketService.sendToSocket("newTrack", JSON.stringify(data));
    }

    getAllTracksFromRoom(){
        return this.tracks;
    }

    clearTracks(){
        console.log('clearing tracks');
        this.tracks = [];
    }

    upVoteTrack(trackId) {
        this.socketService.sendToSocket("upVoteTrack", trackId);
    }

    voteKickTrack(trackId) {
        this.socketService.sendToSocket("voteKickTrack", trackId);
    }

    voteKickAccept(trackId) {
        let vote = {trackId: trackId, kick: true};
        this.socketService.sendToSocket("voteKick", JSON.stringify(vote));
    }

    voteKickDecline(trackId){
        let vote = {trackId: trackId, kick: false};
        this.socketService.sendToSocket("voteKick", JSON.stringify(vote));
    }

    voteNextAccept() {
        let vote = {next: true};
        this.socketService.sendToSocket("voteNext", JSON.stringify(vote));
    }

    voteNextDecline(){
        let vote = {next: false};
        this.socketService.sendToSocket("voteNext", JSON.stringify(vote));
    }
}