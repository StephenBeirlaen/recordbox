import {Injectable, EventEmitter} from '@angular/core';
import * as io from 'socket.io-client';
import {environment} from "../../environments/environment";
import {SocketService} from "./socket.service";

@Injectable()
export class ChatService {
    emitter: EventEmitter<any> = new EventEmitter();

    constructor(private socketService: SocketService){
        this.socketService.usersEmitter.subscribe(event => {
            if(event.eventName === "newUser"){
                this.emitter.emit({ eventName: "logUserJoined", userName: event.user.userName});
                console.log('user joined: ', event.user.userName);
            }
            if(event.eventName === "userLeft"){
                this.emitter.emit({ eventName: "logUserLeft", userName: event.user});
                console.log('user left: ', event.user);
            }
        });

        this.socketService.chatEmitter.subscribe(event => {
            this.emitter.emit({ eventName: "chatMessageReceived", userName: event.userName, message: event.message});
        });
    }

    sendMessage(message){
        this.socketService.sendToSocket('chatMessage', message);
    }
}