import { Injectable } from '@angular/core';
import { Http, Headers} from '@angular/http';

import 'rxjs/add/operator/toPromise';
import {environment} from "../../environments/environment";

@Injectable()
export class ApiClientService {
    private apiUrl = environment.apiURL;

    constructor(private http: Http){}

    getData(path : string){
        return this.http
            .get(
                this.apiUrl + path
            )
            .toPromise()
            .then(res => {
                return res.json();
            }).catch(err => {console.log(err);})
    }

    postData(path: string, data, callback){
        return this.http
            .post(
                this.apiUrl + path,
                data
            )
            .toPromise()
            .then(res => {
                console.log(res);
                callback(null, res);
            })
            .catch(err => {
                console.log(err);
                callback(err, null);
            });
    }
}