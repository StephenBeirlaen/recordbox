import { Injectable, EventEmitter } from '@angular/core';
import { Http, Headers} from '@angular/http';

import 'rxjs/add/operator/toPromise';
import {Jukebox} from "../models/rooms";
import {ApiClientService} from "./api-client.service";
import {SocketService} from "./socket.service";
import {environment} from "../../environments/environment";

@Injectable()
export class JukeboxService {
    //this has to become environment variable in story 2461

    apiService : ApiClientService;
    jukebox: Jukebox = new Jukebox();
    emitter: EventEmitter<any> = new EventEmitter();

    constructor(apiService: ApiClientService, private socketService: SocketService){
        this.apiService = apiService;

        this.socketService.jukeboxEmitter.subscribe(event => {
            if(event.eventName == "newJukeboxName") {
                this.emitter.emit({eventName:"newJukeboxName", jukeboxName: event.jukeboxName});
            }
        });
    }

    createNewJukebox(name: string) {
        let that = this;
        if(name !== ""){
            console.log("creating a new jukebox");
            this.apiService.postData('jukeboxes', JSON.stringify({name: name}), function (err, res) {
                let decodedResponse = JSON.parse(res._body);

                if (decodedResponse &&
                    decodedResponse.hasOwnProperty("_id")) {
                    that.emitter.emit({ eventName: "jukeboxCreated", jukeboxId: decodedResponse._id });
                }
            });
        }
    }

    getRooms(){
        this.apiService.getData('jukeboxes');
    }
}