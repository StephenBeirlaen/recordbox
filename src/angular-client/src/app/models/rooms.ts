export class Jukebox {
    jukeboxId : string;
    name: string;
    userCount : number;
    isSecured : boolean;
}
