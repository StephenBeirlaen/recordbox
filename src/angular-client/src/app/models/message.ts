export class Message{
    constructor(message: string, userName: string){
        this.userName = userName;
        this.content = message;
    }

    userName : string;
    content: string;
}