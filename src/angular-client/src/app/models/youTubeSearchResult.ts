export class YouTubeSearchResult{

    constructor(youtubeId: string, title: string, description: string, thumbnailUrl: string) {
        this.youtubeId = youtubeId;
        this.title = title;
        this.description = description;
        this.thumbnailUrl = thumbnailUrl;
    }

    youtubeId: string;
    title : string;
    description: string;
    thumbnailUrl: string;
}