import { Component, ViewEncapsulation } from '@angular/core';
import {SocketService} from "../services/socket.service";
import {TrackService} from "../services/track.service";
@Component({
    selector: 'youtube',
    templateUrl: './youtube.component.html',
    styleUrls: ['./youtube.component.scss']
})
export class YoutubeComponent{
    private player;
    private songIdPlaying = null;
    private ytEvent;
    private ytLoadingAt;
    btnDisabled: boolean = true;

    waitingToPlay;

    constructor(private trackService: TrackService, private socketService : SocketService){
        //check for when the track starts playing
        this.trackService.playerEmitter.subscribe(event => {
            if(event.eventName == "startPlaying"){
                console.log("start", event.trackId);
                if(!this.player){
                    this.waitingToPlay = event.trackId;
                    return;
                }
                this.startPlaying(event.trackId);
            }
            if(event.eventName == "resumePlaying"){
                console.log("resuming track ", event.trackId,"at ", event.timer);
                this.ytLoadingAt = event.timer;
                if(!this.player){
                    this.waitingToPlay = event.trackId;
                    return;
                }
                this.startPlaying(event.trackId);
            }
        });
        this.trackService.tracksEmitter.subscribe(event => {
            if(event.eventName == 'newTrackList')
            {
                if(event.tracks.length === 0){
                    this.player.loadVideoById(null);
                    this.player.stopVideo();
                }
            }
        });
    }

    onStateChange(event) {
        this.ytEvent = event.data;
    }
    savePlayer(player) {
        this.player = player;
        this.player.loadVideoById(this.songIdPlaying);
        this.player.stopVideo();
        this.btnDisabled = false;
        if(this.waitingToPlay){
            this.startPlaying(this.waitingToPlay);
        }
    }

    playVideo() {
        this.socketService.sendToSocket("startPlayingSong", this.songIdPlaying);
    }

    startPlaying(id){
        if (!id)
            return;
        if(this.ytLoadingAt){
            this.player.loadVideoById(id, this.ytLoadingAt+2);
            this.ytLoadingAt = null;
        }
        else {
            this.player.loadVideoById(id);
        }
        this.player.addEventListener("onStateChange",function(event){
            if(event.data == 2){
                console.log("user trying to stop video.");
                this.player.playVideo();
            }
        }.bind(this));
    }

    disableControls(){
        var iframe = document.getElementsByTagName("iframe")[0];
        iframe.src += ";controls=0";
    }

    pauseVideo() {
        this.player.pauseVideo();
    }
}