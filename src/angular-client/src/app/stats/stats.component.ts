import {Component, OnInit,ViewEncapsulation} from '@angular/core';
import {Router, Params, ActivatedRoute} from "@angular/router";

import { Track } from '../models/track';
import Socket = SocketIOClient.Socket;
import {ApiClientService} from "../services/api-client.service";

@Component({
    selector: 'stats',
    templateUrl: 'stats.component.html',
    styleUrls: ['stats.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class StatsComponent implements OnInit{

    mostPlayedTracks: Track[];
    mostUpvotedTracks: Track[];
    apiService : ApiClientService;

    constructor(private route: ActivatedRoute, private router: Router, apiService: ApiClientService)
    {
        this.apiService = apiService;
    }

    ngOnInit() {
        this.apiService.getData("tracks?order=plays")
            .then(res => {
                if(res){
                    this.mostPlayedTracks = res;
                }
            });
        this.apiService.getData("tracks?order=upvotes")
            .then(res => {
                if(res){
                    this.mostUpvotedTracks = res;
                }
            });
    }
}