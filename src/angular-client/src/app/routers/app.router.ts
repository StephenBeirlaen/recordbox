import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from '../root/app.component';
import {BrowseRoomsComponent} from "../browse-rooms/browse-rooms.component";
import {HomePageComponent} from "../home-page/home-page.component";
import {OneRoomComponent} from "../one-room/one-room.component";
import {LoginGuard} from "../guards/login.guard";
import {StatsComponent} from "../stats/stats.component";
import {EnterUsernamePageComponent} from "../enter-username-page/enter-username-page.component";

const routes: Routes = [
    { path: '', component: HomePageComponent, pathMatch: 'full'},
    { path: 'jukeboxes/:id', component: OneRoomComponent, canActivate: [LoginGuard]},
    { path: 'enter_username/:id', component: EnterUsernamePageComponent},
    { path: 'stats', component: StatsComponent}
];
@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}

