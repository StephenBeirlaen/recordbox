/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import {ActionsComponent} from "./actions.component";
import {SocketService} from "../services/socket.service";
import {TrackService} from "../services/track.service";
import {UserService} from "../services/user.service";

describe('ActionsComponent', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                ActionsComponent
            ],
            imports: [
                RouterTestingModule
            ],
            providers: [
                SocketService,
                TrackService,
                UserService
            ]
        });
    });

    it('should create the component', async(() => {
        let fixture = TestBed.createComponent(ActionsComponent);
        let component = fixture.debugElement.componentInstance;
        expect(component).toBeTruthy();
    }));

    it(`should have an array of tracks'`, async(() => {
        let fixture = TestBed.createComponent(ActionsComponent);
        let component = fixture.debugElement.componentInstance;
        expect(typeof(component.tracks)).toEqual(typeof([]));
    }));

    it('should render playlist-section', async(() => {
        let fixture = TestBed.createComponent(ActionsComponent);
        fixture.detectChanges();
        let compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('div.playlist-section')).not.toBeNull();
    }));
});
