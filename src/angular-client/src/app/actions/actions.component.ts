import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {SocketService} from "../services/socket.service";
import {TrackService} from "../services/track.service";
import {UserService} from "../services/user.service";

@Component({
    selector: 'actions',
    templateUrl: 'actions.component.html',
    styleUrls: ['actions.component.scss']
})
export class ActionsComponent implements OnInit{

    tracks = [];

    constructor(private router: Router, private socketService: SocketService,private trackService:TrackService, private userService: UserService){

    }

    ngOnInit(){
        this.trackService.tracksEmitter.subscribe(event => {
            if(event.eventName == 'newTrackList')
            {
                this.tracks = event.tracks;
            }
        });
    }

    voteNextTrack() {
        this.socketService.sendToSocket("voteNextTrack", null);
    }

    disconnect() {
        this.userService.logout();
    }

    statsPage(){
        window.open('../stats/','_blank');
    }

    download()
    {
        var txt = "";
        //itirate over tracks + add them to filestring
        for(var i = 0;i<this.tracks.length;i++){
            txt += this.tracks[i].title + "\r\n";
        }

        var filename = 'playlist.txt';
        var blob = new Blob([txt], { "type": 'text/plain;charset=utf-8;' });
        if (navigator.msSaveBlob)
        { // IE 10+
            navigator.msSaveBlob(blob, filename);
        }
        else //create a link and click it
        {
            var link = document.createElement("a");
            if (link.download !== undefined) // feature detection
            {
                // Browsers that support HTML5 download attribute
                var url = URL.createObjectURL(blob);
                link.setAttribute("href", url);
                link.setAttribute("download", filename);
                link.style.visibility = 'hidden';
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
    }
}