import {Component, OnInit, forwardRef, Inject} from '@angular/core';

import { Jukebox } from '../models/rooms';
import {ApiClientService} from "../services/api-client.service";
import {LoginRoomComponent} from "../login-room/login-room.component";
import {UserService} from "../services/user.service";
import {Router} from "@angular/router";

@Component({
    selector: 'roomlist',
    templateUrl: './browse-rooms.component.html',
    styleUrls: ['./browse-rooms.component.scss']
})
export class BrowseRoomsComponent implements OnInit{
    jukeboxes: Jukebox[];
    apiService : ApiClientService;
    login : any;

    constructor(@Inject(forwardRef(() => LoginRoomComponent)) login:LoginRoomComponent, apiService: ApiClientService, private userService: UserService, private router: Router){
        this.apiService = apiService;
        this.login = login;
    }

    ngOnInit(){
        console.log("fetching data");
        this.apiService.getData("jukeboxes")
            .then(res => {
                if(res){
                    this.jukeboxes = res;
                }
            });
    }

    selectJukebox(jukeboxId){
        //do something to join the jukebox
        if(!this.login.getUserName()){
            return;
        }else{
            this.userService.setUserName(this.login.getUserName());
            this.router.navigate(['/jukeboxes/', jukeboxId]);
        }
    }
}