import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'login-room',
    templateUrl: './login-room.component.html',
    styleUrls: ['./login-room.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class LoginRoomComponent{
    isRoomlistHidden:boolean = true;
    public displayName : string;

    constructor(){
    }

    getUserName(){
        return this.displayName;
    }
}