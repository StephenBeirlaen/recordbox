import { Component, OnInit } from '@angular/core';
import {TrackService} from "../services/track.service";

@Component({
    selector: 'tracks',
    templateUrl: './tracks.component.html',
    styleUrls: ['./tracks.component.scss']
})
export class TracksComponent implements OnInit{

    tracks = [];
    songIdPlaying: string = "";

    constructor(private trackService: TrackService){

    }

    ngOnInit(){
        //catch event for updating the tracks
        this.trackService.tracksEmitter.subscribe(event => {
            if(event.eventName == 'newTrackList')
            {
                this.tracks = event.tracks;
            }
        });

        this.trackService.playerEmitter.subscribe(event => {
            if(event.eventName == "startPlaying"){
                this.songIdPlaying = event.trackId;
            }
        })
    }

    upVoteTrack(trackId, trackTitle) {
        this.trackService.upVoteTrack(trackId);
        this.trackService.tracksEmitter.emit({ eventName: "logUserUpVoted", trackTitle: trackTitle});
    }

    voteKickTrack(trackId) {
        this.trackService.voteKickTrack(trackId);
    }


}