import {Component, OnInit,ViewEncapsulation} from '@angular/core';
import {Router, Params, ActivatedRoute} from "@angular/router";

import 'rxjs/add/operator/switchMap';
import {ChatService} from "../services/chat.service";

import { Message } from '../models/message';
import { Track } from '../models/track';
import {UserService} from "../services/user.service";
import {TrackService} from "../services/track.service";
import {SocketService} from "../services/socket.service";
import {JukeboxService} from "../services/jukebox.service";
import Socket = SocketIOClient.Socket;

@Component({
    selector: 'one-room',
    templateUrl: './one-room.component.html',
    styleUrls: ['./one-room.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class OneRoomComponent implements OnInit{
    messages : Array<Message>;
    tracks : Array<Track>;
    writtenMessage: string;
    user: string;
    jukeboxName: string = "Loading...";
    trackTitle: string = "No tracks playing";
    inputField: any;
    users: any[] = [];
    voteKickScreenVisible: boolean = false;
    voteInitiator: string;
    trackToKickId: string;
    trackToKickTitle: string;
    voteNextScreenVisible: boolean = false;
    voted = false;
    voteMessage;

    constructor(private route: ActivatedRoute, private router: Router, private chatService: ChatService, private userService: UserService, private trackService: TrackService, private socketService: SocketService, private jukeboxService: JukeboxService)
    {
        this.messages = new Array<Message>();
    }

    ngOnInit() {
        //check if user has username / check if user is logged in
        this.user = this.userService.username;
        // Add initial message
        let welcomeMsg = new Message("Welcome to this jukebox. To get started, add a track on the left. Have fun!", this.user);
        this.messages.push(welcomeMsg);
        //fetch jukebox id
        let jukeBoxId = this.route.snapshot.params['id'];
        //login the user to the jukebox
        this.userService.login(this.userService.username, jukeBoxId);
        //get inputfield to style or disable when needed
        this.inputField = document.getElementById("writtenMessage");
        //get users and tracks from the services
        this.users = this.userService.users;
        this.tracks = this.trackService.tracks;
        
        //chat service listen to new messages
        this.chatService.emitter.subscribe(event => {
            let mes;
            switch (event.eventName) {
                case "chatMessageReceived":
                    mes = new Message(event.userName + ": " + event.message, event.userName);
                    this.messages.push(mes);
                    if (event.userName == this.userService.username) { // if it was the user's message
                        this.writtenMessage = '';
                        this.inputField.disabled = false;
                        this.inputField.focus();
                    }
                    this.scrollDown();
                    break;
                case "logUserJoined":
                    //add user to user list
                    mes = new Message("New user " + event.userName + " entered the jukebox", event.userName);
                    this.messages.push(mes);
                    this.scrollDown();
                    break;
                case "logUserLeft":
                    //add user to user list
                    mes = new Message(event.userName + " has left the jukebox", event.userName);
                    this.messages.push(mes);
                    this.scrollDown();
                    break;
            }
        });

        this.trackService.playerEmitter.subscribe(event => {
            //check for when the track starts playing
            if(event.eventName == "startPlaying"){
                this.trackTitle = event.title;
            }
        });

        this.trackService.tracksEmitter.subscribe(event => {
            if(event.eventName == "voteKickTrack"){
                this.voteInitiator = event.userName;
                this.trackToKickId = event.trackId;
                this.trackToKickTitle = event.trackTitle;
                this.voteKickScreenVisible = true;

                let mes = new Message(this.voteInitiator + " wants to kick " + this.trackToKickTitle,"");
                this.messages.push(mes);
                this.scrollDown();
            }
            if(event.eventName == "kickVoteDone"){
                this.voteKickScreenVisible = false;
                this.voted = false;
                this.voteMessage = '';
                let mes;
                if(event.kick){
                    mes = new Message(this.trackToKickTitle + " was kicked!"," :(");
                } else {
                    mes = new Message(this.trackToKickTitle + " was not kicked.", ":)");
                }
                this.messages.push(mes);
                this.scrollDown();
            }
            if(event.eventName == "voteNextTrack"){
                this.voteInitiator = event.userName;
                this.trackToKickTitle = this.trackTitle;
                this.voteNextScreenVisible = true;

                let mes = new Message(this.voteInitiator + " wants to skip " + this.trackToKickTitle,"");
                this.messages.push(mes);
                this.scrollDown();
            }
            if(event.eventName == "nextTrackVoteDone"){
                this.voteNextScreenVisible = false;
                this.voted = false;
                this.voteMessage = '';

                let mes;
                if(event.next){
                    mes = new Message(this.trackToKickTitle + " was skipped!"," :(");
                } else {
                    mes = new Message(this.trackToKickTitle + " was not skipped.", ":)");
                }
                this.messages.push(mes);
                this.scrollDown();
            }
            if (event.eventName == "logUserUpVoted") {
                // bevestiging tonen, er is iets van bevestiging nodig voor integration testing
                let mes = new Message("You upvoted the track: " + event.trackTitle, "");
                this.messages.push(mes);
                this.scrollDown();
            }
        });

        //catch event for updating the users
        this.userService.usersEmitter.subscribe(event => {
            if(event.eventName === "userList"){
                this.users = event.users;
            }
        });

        this.jukeboxService.emitter.subscribe(event => {
            if(event.eventName == "newJukeboxName"){
                this.jukeboxName = event.jukeboxName;
            }
        });
    }

    sendMessage(){
        if(!this.writtenMessage){
            return;
        }
        this.inputField.disabled = true;
        let message : Message = {'content': this.writtenMessage, 'userName': this.user};
        this.chatService.sendMessage(message.content);
        this.writtenMessage = 'sending';
    }

    voteKick(){
        this.voted = true;
        this.voteMessage = 'You voted to kick, waiting for other votes...';
        this.trackService.voteKickAccept(this.trackToKickId);
    }

    declineKick(){
        this.voted = true;
        this.voteMessage = 'You voted to kick, waiting for other votes...';
        this.trackService.voteKickDecline(this.trackToKickId);
    }

    voteNextTrack(){
        this.voted = true;
        this.voteMessage = 'You voted to skip the current track, waiting for other votes...';
        this.trackService.voteNextAccept();
    }

    declineNextTrack(){
        this.voted = true;
        this.voteMessage = 'You voted to keep playing the current track, waiting for other votes...';
        this.trackService.voteNextDecline();
    }

    scrollDown(){
        var element = document.getElementById('chatSection');
        setTimeout(()=>{element.scrollTop = (element.scrollHeight);}, 20);
    }
}