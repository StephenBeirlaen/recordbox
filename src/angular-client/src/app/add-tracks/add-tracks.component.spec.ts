/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import {AddTracksComponent} from "./add-tracks.component";
import {TrackService} from "../services/track.service";
import {YouTubeService} from "../services/youtube.service";
import {YouTubeSearchResult} from "../models/youTubeSearchResult";
import {FormsModule} from "@angular/forms";
import {SocketService} from "../services/socket.service";
import {HttpModule} from "@angular/http";

describe('AddTracksComponent', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                AddTracksComponent
            ],
            imports: [
                FormsModule,
                HttpModule
            ],
            providers: [
                SocketService,
                TrackService,
                YouTubeService
            ]
        });
    });

    it('should create the component', async(() => {
        let fixture = TestBed.createComponent(AddTracksComponent);
        let component = fixture.debugElement.componentInstance;
        expect(component).toBeTruthy();
    }));

    /*it(`should have a trackId field'`, async(() => {
        let fixture = TestBed.createComponent(AddTracksComponent);
        let component = fixture.debugElement.componentInstance;
        expect(typeof(component.trackId)).toEqual(typeof(String));
    }));

    it(`should have a trackUrl field'`, async(() => {
        let fixture = TestBed.createComponent(AddTracksComponent);
        let component = fixture.debugElement.componentInstance;
        expect(typeof(component.trackUrl)).toEqual(typeof(String));
    }));

    it(`should have a searchQuery field'`, async(() => {
        let fixture = TestBed.createComponent(AddTracksComponent);
        let component = fixture.debugElement.componentInstance;
        expect(typeof(component.searchQuery)).toEqual(typeof(String));
    }));

    it(`should have an array of youtube search results'`, async(() => {
        let fixture = TestBed.createComponent(AddTracksComponent);
        let component = fixture.debugElement.componentInstance;
        expect(typeof(component.searchResults)).toEqual(typeof([]));
    }));*/

    it('should render playlist-section', async(() => {
        let fixture = TestBed.createComponent(AddTracksComponent);
        fixture.detectChanges();
        let compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('div.playlist-section')).not.toBeNull();
    }));
});
