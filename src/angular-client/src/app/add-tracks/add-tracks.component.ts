import { Component, OnInit } from '@angular/core';
import {TrackService} from "../services/track.service";
import {YouTubeService} from "../services/youtube.service";
import {YouTubeSearchResult} from "../models/youTubeSearchResult";

@Component({
    selector: 'addtracks',
    templateUrl: 'add-tracks.component.html',
    styleUrls: ['add-tracks.component.scss']
})
export class AddTracksComponent implements OnInit{

    trackId: string;
    trackUrl: string;
    searchQuery: string;
    searchResults: YouTubeSearchResult[];

    constructor(private trackService: TrackService, private youTubeSearchService: YouTubeService){

    }

    ngOnInit(){

    }

    searchTracks() {
        this.youTubeSearchService.searchYoutube(this.searchQuery)
            .subscribe(r => this.searchResults = r)
        this.searchQuery = "";
    }

    addNewTrackById(id){
        if(!id){
            console.log("invalid id");
            return;
        }
        this.trackService.addTrackToPlaylist(id);
        this.trackId = id;
    }

    youtubeUrlParser(url) { // http://stackoverflow.com/questions/3452546/javascript-regex-how-to-get-youtube-video-id-from-url
        var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
        var match = url.match(regExp);
        return (match&&match[7].length==11)? match[7] : false;
    }

    addNewTrackByUrl(url) {
        if(!url){
            console.log("invalid url");
            return;
        }
        this.addNewTrackById(this.youtubeUrlParser(url));
        this.trackUrl = "";
    }
}