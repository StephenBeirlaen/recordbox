import { browser, element, by } from 'protractor';

export class AngularStartProjectPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }

  getTitle() {
    return element(by.tagName('title')).getText();
  }
}
