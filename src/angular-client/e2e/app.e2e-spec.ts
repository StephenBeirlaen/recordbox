import {AngularStartProjectPage} from './app.po';
import {browser, element, by, protractor, promise} from "protractor";

describe('RecordBox front-end', function () {
    let page: AngularStartProjectPage;
    let EC = protractor.ExpectedConditions;

    beforeEach(() => {
        browser.ignoreSynchronization = true; // voor sockets.io https://github.com/angular/angular/issues/11853
        page = new AngularStartProjectPage();
    });

    it('should display title saying RecordBox', () => {
        page.navigateTo();
        expect(page.getParagraphText()).toEqual('RecordBox');
    });

    describe("home page", function () {
        let jukeboxes;
        let username;

        it('should be able to open the jukebox browser', () => {
            browser.get("/");

            // click jukebox browser button
            let browseRoomsButton = element(
                by.css("button.btn-browse-rooms"));
            browseRoomsButton.click();

            // wait until it has opened (necessary to click the first one)
            var jukeboxList = element(by.css('roomlist'));
            browser.wait(EC.visibilityOf(jukeboxList), 5000);

            // check jukeboxes
            jukeboxes = element.all(by.css("roomlist ul li"));
            expect(jukeboxes.count()).toBeGreaterThan(0);
        });

        it('should be able to log in', () => {
            // enter user name
            let displayNameInput = element(
                by.css("input[type=text]#display-name"));
            username = "IntegrationTestUser1";
            displayNameInput.sendKeys(username);

            // log into the first jukebox
            jukeboxes.first().click();

            // wait for loading of user list
            let userList = element(by.css("section.right ul"));
            browser.wait(EC.presenceOf(userList), 5000);
        });

        it('should be able to show the users', () => {
            // check users
            let users = element.all(by.css("section.right ul li"));
            expect(users.count()).toBeGreaterThan(0);

            // check the test user is in the list
            let userEntry = users.last();
            expect(userEntry.getText()).toEqual(username);
        });

        it('should be able to log out', () => {
            // click leave jukebox button
            let leaveJukeboxButton = element(
                by.css("button.btn-sign-out"));
            leaveJukeboxButton.click();

            // wait for loading of the splash page
            let browseRoomsButton = element(by.css("button.btn-browse-rooms"));
            browser.wait(EC.presenceOf(browseRoomsButton), 5000);
        });
    });

    describe("jukebox", function () {
        let username;

        it('should be able to log in', () => {
            browser.get("/");

            // click jukebox browser button
            let browseRoomsButton = element(
                by.css("button.btn-browse-rooms"));
            browseRoomsButton.click();

            // wait until it has opened (necessary to click the first one)
            var jukeboxList = element(by.css('roomlist'));
            browser.wait(EC.visibilityOf(jukeboxList), 5000);

            // check jukeboxes
            let jukeboxes = element.all(by.css("roomlist ul li"));

            // enter user name
            let displayNameInput = element(
                by.css("input[type=text]#display-name"));
            let displayname = "IntegrationTestUser1";
            displayNameInput.sendKeys(displayname);

            // log into the first jukebox
            jukeboxes.first().click();

            // wait for loading of user list
            let userList = element(by.css("section.right ul"));
            browser.wait(EC.presenceOf(userList), 5000);

            let users = element.all(by.css("section.right ul li"));
            users.last().getText().then(function(name) {
                username = name;
                expect(username).toEqual(displayname);
            });
        });

        it('should be able to send a chat', () => {
            let chatInput = element(
                by.css("input[type=text]#writtenMessage"));
            let chat = "Test bericht";
            chatInput.sendKeys(chat);

            // click chat send button
            let sendChatButton = element(
                by.css("button.btn-send-chat"));
            sendChatButton.click();

            // wait until it has opened (necessary to click the first one)
            /*var chatMessages = element.all(by.css("div.comment-section div"));
            var chatCount = chatMessages.count();
            browser.wait(
                function () {
                    return (element.all(by.css("div.comment-section div"))).count() != chatCount
                }, 5000);*/

            /*browser.wait(
                function () {
                    var deferred = protractor.promise.defer();
                    (element.all(by.css("div.comment-section div"))).count()
                        .then(function (rows) {
                        deferred.fulfill(function (rows) {
                            return rows.length === chatCount;
                        });
                });
                return deferred.promise;
            }, 5000);*/
            browser.sleep(2000);

            var chatMessages = element.all(by.css("div.comment-section div"));
            chatMessages.last().element(by.tagName("span")).getText().then(function(chatmsg) {
                expect(username + ": " + chat).toEqual(chatmsg);
            });
        });

        it('should be able to add a track', () => {
            let trackUrlInput = element(by.name("trackId"));
            let url = "https://www.youtube.com/watch?v=kfVsfOSbJY0";
            trackUrlInput.sendKeys(url);

            let addByUrlButton = element(by.css("button.btn-add-url"));
            addByUrlButton.click();

            browser.sleep(2000);

            let tracks = element.all(by.css("section.left div.playlist-section li"));
            let correctTrack = tracks.all(by.cssContainingText('span', 'Friday - Rebecca Black - Official Music Video')).first();
            correctTrack.getText().then(function(title) {
                expect(title).toEqual("Friday - Rebecca Black - Official Music Video");
            });
        });

        it('should be able to add a track from YouTube search', () => {
            let trackSearchInput = element(by.name("searchQuery"));
            let query = "Stavroz The Finishing Original Mix";
            trackSearchInput.sendKeys(query);

            let searchYouTubeButton = element(by.css("button.btn-search-youtube"));
            searchYouTubeButton.click();

            browser.sleep(2000);

            let searchResults = element.all(by.css("section.left div.search-results li"));
            let correctResult = searchResults.all(by.cssContainingText('span', 'Stavroz - The Finishing (Original Mix)')).first();
            correctResult.getText().then(function(title) {
                expect(title).toEqual("Stavroz - The Finishing (Original Mix)");
            });

            correctResult.click();

            browser.sleep(1000);

            let tracks = element.all(by.css("section.left div.playlist-section li"));
            let correctTrack = tracks.all(by.cssContainingText('span', 'Stavroz - The Finishing (Original Mix)')).first();
            correctTrack.getText().then(function(title) {
                expect(title).toEqual("Stavroz - The Finishing (Original Mix)");
            });
        });

        it('should be able to upvote a track', () => {
            let tracks = element.all(by.css("section.left div.playlist-section li"));
            let correctTrack = tracks.all(by.cssContainingText('span', 'Stavroz - The Finishing (Original Mix)')).first();
            correctTrack.getText().then(function(title) {
                expect(title).toEqual("Stavroz - The Finishing (Original Mix)");
            });
            // get the next element (the buttons)
            let trackBtns = correctTrack.element(by.xpath('following-sibling::span'));
            let thumbsUpButton = trackBtns.all(by.tagName("i")).first();
            thumbsUpButton.click();

            browser.sleep(2000);

            var chatMessages = element.all(by.css("div.comment-section div"));
            chatMessages.last().element(by.tagName("span")).getText().then(function(chatmsg) {
                expect("You upvoted the track: Stavroz - The Finishing (Original Mix)").toEqual(chatmsg);
            });
        });

        it('should be able to download the tracklist', () => {
            let actionButtons = element.all(by.css("section.right div.playlist-section button"));
            let downloadTracklistButton = actionButtons.all(by.cssContainingText('span', 'Download playlist')).first();

            downloadTracklistButton.click();

            // todo: how to test for file downloads?
        });

        it('should be able to log out', () => {
            // click leave jukebox button
            let leaveJukeboxButton = element(
                by.css("button.btn-sign-out"));
            leaveJukeboxButton.click();

            // wait for loading of the splash page
            let browseRoomsButton = element(by.css("button.btn-browse-rooms"));
            browser.wait(EC.presenceOf(browseRoomsButton), 5000);
        });
    });
});
