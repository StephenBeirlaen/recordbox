"use strict";

// include gulp
var gulp = require("gulp"),
    child_process = require("child_process"),
    spawn = require('child_process').spawn, // https://gist.github.com/webdesserts/5632955
    node,
    htmlhint = require("gulp-htmlhint"),
    sourcemaps = require("gulp-sourcemaps"),
    autoprefixer = require("gulp-autoprefixer"),
    cleanCSS = require("gulp-clean-css"),
    concat = require("gulp-concat"),
    csslint = require("gulp-csslint"),
    sassLint = require("gulp-sass-lint"),
    jshint = require("gulp-jshint"),
    jsStylish = require("jshint-stylish"),
    uglify = require("gulp-uglify"),
    sass = require("gulp-sass"),
    babel = require("gulp-babel"),
    exec = require("gulp-exec"),
    mocha = require('gulp-mocha'),
    gulputil = require('gulp-util');

const PATHS = { // const - hier mag je gerust ES6 gebruiken, deze file wordt niet in browser gebruikt.
    NODE_JS: {
        SRC: './src/server/scripts/**/*.js'
    },
    HTML: {
        SRC: './src/server/public/wwwroot/**/*.html'
    },
    SCSS: {
        SRC: './src/server/public/css/**/*.scss',
        DEST: './src/server/public/wwwroot/css/'
    },
    ANGULAR: {
        SRC: './src/angular-client/',
        DEST: './src/server/public/wwwroot'
    }
};

// PUBLIC_JS hint task
gulp.task('copy-externals', function() { // dist folder van bower_components nr lib in wwwroot kopieren.

});

gulp.task('html-validate', function () {
    gulp.src(PATHS.HTML.SRC)
        .pipe(htmlhint('.htmlhintrc')) // validaten met config
        .pipe(htmlhint.reporter('htmlhint-stylish')) // rapportje
        .pipe(htmlhint.failReporter());
});

// automatiseer deze validatie taak
gulp.task('start-watch', function () { // elke keer als je file opslaat zal deze taak starten
    var htmlWatcher = gulp.watch(PATHS.HTML.SRC, ['html-validate']);
    htmlWatcher.on('change', function (event) {
        console.log("file: " + event.path + " was " + event.type);
    });
    var scssWatcher = gulp.watch(PATHS.SCSS.SRC, ['scss']);
    scssWatcher.on('change', function (event) {
        console.log("file: " + event.path + " was " + event.type);
    });
    var publicjsWatcher = gulp.watch(PATHS.PUBLIC_JS.SRC, ['public_js']);
    publicjsWatcher.on('change', function (event) {
        console.log("file: " + event.path + " was " + event.type);
    });
    var nodejsWatcher = gulp.watch(PATHS.NODE_JS.SRC, ['node_js', 'node_run']); // node server herstarten
    nodejsWatcher.on('change', function (event) {
        console.log("file: " + event.path + " was " + event.type);
    });

    gulp.start('node_run'); // eerste keer starten
});


const AUTOPREFIXOPTIONS = {
    browsers: ["last 2 versions"]
};

gulp.task('scss', function () {
    gulp.src(PATHS.SCSS.SRC)
        .pipe(sourcemaps.init())
        .pipe(sassLint({
            options: {
                formatter: 'stylish',
                'merge-default-rules': false
            }
        }))
        .pipe(sassLint.format())
        .pipe(sassLint.failOnError())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer(AUTOPREFIXOPTIONS))
        .pipe(csslint({
            "adjoining-classes": false,
            "box-model": false,
            "box-sizing": false,
            "duplicate-background-images": false,
            "ids": false,
            "order-alphabetical": false,
            "qualified-headings": false,
            "unique-headings": false,
            "universal-selector": false
        }))
        .pipe(csslint.formatter())
        .pipe(concat("main.min.css"))
        .pipe(cleanCSS({debug: true, compatibility: '*'}, function (details) {
            console.log(details.name + " oorspronkelijk: " + details.stats.originalSize);
            console.log(details.name + " minified:       " + details.stats.minifiedSize);
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(PATHS.SCSS.DEST));
});

gulp.task('public_js', function () {
    gulp.src(PATHS.PUBLIC_JS.SRC) // src objects
        .pipe(jshint()) // jslint
        .pipe(jshint.reporter(jsStylish))
        .pipe(sourcemaps.init())// sourcemap init
        .pipe(babel())
        .pipe(concat("app.min.js")) // concat
        .pipe(uglify()) // uglify
        .pipe(sourcemaps.write()) // sourcemaps write
        .pipe(gulp.dest(PATHS.PUBLIC_JS.DEST)); // destination
});

gulp.task('node_js', function () {
    gulp.src(PATHS.NODE_JS.SRC) // src objects
        .pipe(jshint()) // jslint
        .pipe(jshint.reporter(jsStylish))
});

gulp.task('default', ['build_angular'], function() {
    if (node) node.kill();
    node = spawn('node', ['src/server/app.js'], {stdio: 'inherit'});
    node.on('close', function (code) {
        if (code === 8) {
            gulp.log('Error detected, waiting for changes...');
        }
    });
});

gulp.task('node', function() {
    if (node) node.kill();
    node = spawn('node', ['src/server/app.js'], {stdio: 'inherit'});
    node.on('close', function (code) {
        if (code === 8) {
            gulp.log('Error detected, waiting for changes...');
        }
    });
});
gulp.task('serve_angular', function () {
    return gulp.src('')
        .pipe(exec('cd src/angular-client & ng s --host 0.0.0.0', {maxBuffer: 1024 * 500}, function (error, stdout, stderr) {
            console.log(error);
            console.log(stdout);
            console.log(stderr);
        }));
});

gulp.task('build_angular', function () {
    return gulp.src('')
        .pipe(exec('cd src/angular-client & ng build', {maxBuffer: 1024 * 500}, function (error, stdout, stderr) {
            console.log(error);
            console.log(stdout);
            console.log(stderr);
        }));
});


gulp.task('node-unit-tests', function () {
    return gulp.src(['src/server/tests/mocha/*.js'], { read: false })
        .pipe(mocha({ reporter: 'list' }))
        .on('error', gulputil.log);
});

gulp.task('angular-unit-tests', function () {
    /*child_process.exec('cd src/angular-client & ng test', {encoding: "ascii", maxBuffer: 1024 * 500}, function (error, stdout, stderr) {
        console.log(error);
        console.log(stdout);
        console.log(stderr);
    });*/ // no output from child process... use spawn:
    var spawn = require('child_process').spawnSync,
    ls = spawn('cd src/angular-client & ng test', {
        shell: true,
        stdio: 'inherit'
    });

    ls.stdout.on('data', function (data) {
        console.log(data.toString());
    });

    ls.stderr.on('data', function (data) {
        console.log('stderr: ' + data.toString());
    });
});

gulp.task('angular-e2e-tests', function () {
    child_process.exec('cd src/angular-client & protractor', {maxBuffer: 1024 * 500}, function (error, stdout, stderr) {
        console.log(error);
        console.log(stdout);
        console.log(stderr);
    });
});

// now using mLab online free 500 mb mongodb
/*gulp.task('run-mongodb', function () {
    child_process.exec('mongod -dbpath "src/server/resources/db"', {maxBuffer: 1024 * 500}, function (error, stdout, stderr) {
        console.log(error);
        console.log(stdout);
        console.log(stderr);
    });
});*/

// clean up if an error goes unhandled.
process.on('exit', function() {
    if (node) node.kill()
});